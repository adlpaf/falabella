# Falabella
# Challenge - Backend Developer

Write a program that prints all the numbers from 1 to 100. However, for
multiples of 3, instead of the number, print "Falabella". For multiples of 5 print
"IT". For numbers which are multiples of both 3 and 5, print "Integraciones".

But here's the catch: you can use only one `if`. No multiple branches, ternary
operators or `else`.

# Requirements
* 1 if
* You can't use `else`, `else if` or ternary
* Unit tests
* Feel free to apply your SOLID knowledge
* You can write the challenge in any language you want. Here at Falabella we are
big fans of PHP, Kotlin and TypeScript
* One git branch for the solution
* One git branch for the documentation.

# Submission
Create a repository on GitLab, GitHub or any other similar service and just send us the link.

# To Install
* Install composer more info in `https://getcomposer.org/`
* Clone the project into webserver www directory: git clone https://gitlab.com/adlpaf/falabella
* Move into directory you've cloned: cd falabella
* Run composer `dump-autoload`
* In browser acces to `localhost/falabella/public/`
* For test in terminal inside your project folder run `vendor/bin/phpunit tests/ --color`
