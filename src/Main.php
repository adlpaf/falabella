<?php

namespace Falabella;

class Main {
    public function runChallenge($first, $last) {
        $calc = new Calc;
        $div31 = new Number(3, 1, 'Falabella');
        $div51 = new Number(5, 1, 'IT');
        $div35 = new Number(5, 3, 'Integraciones');

        $returnString = "";

        for ($i = $first; $i <= $last; $i++) {
            $result = $i;
            $result = $calc->getResult($result,$div35);
            $result = $calc->getResult($result,$div31);
            $result = $calc->getResult($result,$div51);
            $returnString .= $i . ": " . $result."<br>";
        }
    
        return $returnString;
    }

}
