<?php

namespace Falabella;
require '../vendor/autoload.php';

$main = new Main;
/**
 * Method to run the challenge
 * It's defined with 1 to 100
 */ 
$result = $main->runChallenge(1,100);

$message = new show;
$message->showMessage($result);
